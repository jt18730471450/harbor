 <!--
    Copyright (c) 2016 VMware, Inc. All Rights Reserved.
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
-->
<!-- Main jumbotron for a primary marketing message or call to action -->
 <div class="jumbotron" id="jumbotron">
   <div class="container">
     <img src="static/resources/image/banner_img.png" alt="Harbor's Logo" height="138" width="680"/>
<!-- 	 <p class="pull-left" style="margin-top: 85px; color: #245580; font-size: 30pt; text-align: center; width: 60%;">{{i18n .Lang "index_title"}}</p> -->
   </div>
 </div>

 <div class="container">
   <!-- Example row of columns -->
   <div class="row">
     <div class="stuff col-md-12" >
        <p>DataFoundry 提供的公共镜像仓库是可靠的企业级 Registry 服务器。使用 DataFoundry 公共镜像仓库来提高生产效率和安全度，即可应用于生产环境，也可以在开发环境中使用。</p>
    </div>
    <div class="des col-md-12">
        <p><span>安全</span>确保知识产权在自己组织内部的管控之下。</p>
        <p><span>效率</span>搭建组织内部的私用容器Registry服务，可显著降低访问公共Registry服务的网络需求。</p>
        <p><span>审计</span>所有访问Registry服务的操作均被记录，便于日后审计。</p>
        <p><span>访问</span>提供基于角色的访问控制，可集成企业目前拥有的用户管理系统(如:AD/LDAP)。</p>
        <p><span>管理</span>具有友好易用图形管理界面。</p>
    </div>
   </div>
 </div> <!-- /container -->

<script src="static/resources/js/login.js"></script>
